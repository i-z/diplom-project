//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InternalDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Card
    {
        public Card()
        {
            this.EntranceHistories = new HashSet<EntranceHistory>();
        }
    
        public int Id { get; set; }
        public string CardId { get; set; }
        public string UserProfileId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedAt { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ICollection<EntranceHistory> EntranceHistories { get; set; }
    }
}
