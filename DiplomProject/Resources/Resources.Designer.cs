﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to За нас.
        /// </summary>
        public static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добре дошли на страниците на ТУ Фитнес. В момента разполагаме с фитнес зали в София, Пловдив и Сливен. Моля регистрирайте си акаунт и заповядайте да се регистрирате в системата за да получите достъп до повече информация. Можете да заплатите фитнес членството си онлайн или да предплатите брой кредити, които да използвате за вход в залата. (Цена на тренировка: {0} кредита). Преди да продължите, имайте предвид, че този фитнес НЕ Е изтински фитнес. Уебсайтът на който в момента се намирате е разработен от Иван З [rest of string was truncated]&quot;;.
        /// </summary>
        public static string AboutContent {
            get {
                return ResourceManager.GetString("AboutContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Акаунт.
        /// </summary>
        public static string Account {
            get {
                return ResourceManager.GetString("Account", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Абонамент, активен до.
        /// </summary>
        public static string ActiveUntil {
            get {
                return ResourceManager.GetString("ActiveUntil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Адрес.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Администрация.
        /// </summary>
        public static string AdminPanel {
            get {
                return ResourceManager.GetString("AdminPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ТУ Фитнес.
        /// </summary>
        public static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Обвържете своя.
        /// </summary>
        public static string AssociateYour {
            get {
                return ResourceManager.GetString("AssociateYour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Форма за обвързване.
        /// </summary>
        public static string AssociationForm {
            get {
                return ResourceManager.GetString("AssociationForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Български.
        /// </summary>
        public static string Bulgarian {
            get {
                return ResourceManager.GetString("Bulgarian", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Данни за картата.
        /// </summary>
        public static string CardData {
            get {
                return ResourceManager.GetString("CardData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Номер на карта.
        /// </summary>
        public static string CardNumber {
            get {
                return ResourceManager.GetString("CardNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Промяна на парола.
        /// </summary>
        public static string ChangePassword {
            get {
                return ResourceManager.GetString("ChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Форма за смяна на паролата.
        /// </summary>
        public static string ChangePasswordForm {
            get {
                return ResourceManager.GetString("ChangePasswordForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Изберете език .
        /// </summary>
        public static string ChooseYourLanguage {
            get {
                return ResourceManager.GetString("ChooseYourLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Потвърдете новата парола.
        /// </summary>
        public static string ConfirmNewPassword {
            get {
                return ResourceManager.GetString("ConfirmNewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Потвърдете паролата.
        /// </summary>
        public static string ConfirmPassword {
            get {
                return ResourceManager.GetString("ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Контакти.
        /// </summary>
        public static string Contact {
            get {
                return ResourceManager.GetString("Contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Създаване на нов акаунт..
        /// </summary>
        public static string CreateANewAccount {
            get {
                return ResourceManager.GetString("CreateANewAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Създайте локален акаунт.
        /// </summary>
        public static string CreateLocalLogin {
            get {
                return ResourceManager.GetString("CreateLocalLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Кредити.
        /// </summary>
        public static string Credits {
            get {
                return ResourceManager.GetString("Credits", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to лева.
        /// </summary>
        public static string Currency {
            get {
                return ResourceManager.GetString("Currency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Текуща парола.
        /// </summary>
        public static string CurrentPassword {
            get {
                return ResourceManager.GetString("CurrentPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата.
        /// </summary>
        public static string Date {
            get {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Регистрирайте се.
        /// </summary>
        public static string DoRegister {
            get {
                return ResourceManager.GetString("DoRegister", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Този Email вече се ползва.
        /// </summary>
        public static string EmailAlreadyInUse {
            get {
                return ResourceManager.GetString("EmailAlreadyInUse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English.
        /// </summary>
        public static string English {
            get {
                return ResourceManager.GetString("English", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Въведете нов номер на карта.
        /// </summary>
        public static string EnterNewCard {
            get {
                return ResourceManager.GetString("EnterNewCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Моля въведете потребителско име за този сайт и натиснете бутона за Регистрация за да създадете своя акунт и да влезете в системата..
        /// </summary>
        public static string EnterUserName {
            get {
                return ResourceManager.GetString("EnterUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Грешка.
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Възникна грешка при обработване на заявката Ви..
        /// </summary>
        public static string ErrorProcessingRequest {
            get {
                return ResourceManager.GetString("ErrorProcessingRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Помещение.
        /// </summary>
        public static string Facility {
            get {
                return ResourceManager.GetString("Facility", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Име.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Здравейте, .
        /// </summary>
        public static string Hello {
            get {
                return ResourceManager.GetString("Hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Начална страница.
        /// </summary>
        public static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ако нямате локален акаунт..
        /// </summary>
        public static string IfYouDontHaveALocalAccount {
            get {
                return ResourceManager.GetString("IfYouDontHaveALocalAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Това не е валиден Email адрес..
        /// </summary>
        public static string InvalidEmail {
            get {
                return ResourceManager.GetString("InvalidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Платил.
        /// </summary>
        public static string Issuer {
            get {
                return ResourceManager.GetString("Issuer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Фамилия.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вход.
        /// </summary>
        public static string LogIn {
            get {
                return ResourceManager.GetString("LogIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Грешка при вход.
        /// </summary>
        public static string LoginFailure {
            get {
                return ResourceManager.GetString("LoginFailure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Изход.
        /// </summary>
        public static string LogOff {
            get {
                return ResourceManager.GetString("LogOff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Управление на акаунт.
        /// </summary>
        public static string ManageAccount {
            get {
                return ResourceManager.GetString("ManageAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Управител.
        /// </summary>
        public static string Manager {
            get {
                return ResourceManager.GetString("Manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Управление.
        /// </summary>
        public static string ManagerPanel {
            get {
                return ResourceManager.GetString("ManagerPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} не трябва да е повече от {1} символа..
        /// </summary>
        public static string MaxLength {
            get {
                return ResourceManager.GetString("MaxLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} трябва да бъде поне {2} символа..
        /// </summary>
        public static string MinLength {
            get {
                return ResourceManager.GetString("MinLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Месец.
        /// </summary>
        public static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Име.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нова парола.
        /// </summary>
        public static string NewPassword {
            get {
                return ResourceManager.GetString("NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Новата парола и паролата за потвърждение не съвпадат..
        /// </summary>
        public static string NewPasswordMismatch {
            get {
                return ResourceManager.GetString("NewPasswordMismatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Няма активен абонамент.
        /// </summary>
        public static string NoActiveSubsription {
            get {
                return ResourceManager.GetString("NoActiveSubsription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нямате &lt;em&gt;потребителско име / парола&lt;/em&gt; за тази система. Създайте акаунт за да можете да влезете в сайта, без използването на външна система..
        /// </summary>
        public static string NoLocalUserName {
            get {
                return ResourceManager.GetString("NoLocalUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Парола.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Паролата и паролата за потвърждение не съвпадат..
        /// </summary>
        public static string PasswordMismatch {
            get {
                return ResourceManager.GetString("PasswordMismatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Плащания.
        /// </summary>
        public static string PaymentsLog {
            get {
                return ResourceManager.GetString("PaymentsLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PayPal Email.
        /// </summary>
        public static string PayPalEmail {
            get {
                return ResourceManager.GetString("PayPalEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Телефон.
        /// </summary>
        public static string Phone {
            get {
                return ResourceManager.GetString("Phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Цена на тренировка.
        /// </summary>
        public static string PracticeCost {
            get {
                return ResourceManager.GetString("PracticeCost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Продукт.
        /// </summary>
        public static string Product {
            get {
                return ResourceManager.GetString("Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Приел.
        /// </summary>
        public static string Recepient {
            get {
                return ResourceManager.GetString("Recepient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Регистрация.
        /// </summary>
        public static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запомни ме ?.
        /// </summary>
        public static string RememberMe {
            get {
                return ResourceManager.GetString("RememberMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Премахни.
        /// </summary>
        public static string Remove {
            get {
                return ResourceManager.GetString("Remove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запазете парола.
        /// </summary>
        public static string SetPassword {
            get {
                return ResourceManager.GetString("SetPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Статут.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Абонамент.
        /// </summary>
        public static string Subscription {
            get {
                return ResourceManager.GetString("Subscription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Успешно се аутентикирахте с.
        /// </summary>
        public static string SuccessfulAuthentication {
            get {
                return ResourceManager.GetString("SuccessfulAuthentication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Време.
        /// </summary>
        public static string Time {
            get {
                return ResourceManager.GetString("Time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сайт на технически университет.
        /// </summary>
        public static string TUSite {
            get {
                return ResourceManager.GetString("TUSite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неограничен достъп.
        /// </summary>
        public static string UnlimitedAccess {
            get {
                return ResourceManager.GetString("UnlimitedAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неуспешен запис на данни.
        /// </summary>
        public static string UnsuccessfulDataSave {
            get {
                return ResourceManager.GetString("UnsuccessfulDataSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неуспешен вход, чрез външна система..
        /// </summary>
        public static string UnsuccessfulLoginWithService {
            get {
                return ResourceManager.GetString("UnsuccessfulLoginWithService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Използвайте локален акаунт за вход в системата..
        /// </summary>
        public static string UseALocalAccountToLogIn {
            get {
                return ResourceManager.GetString("UseALocalAccountToLogIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Използвайте друга система за достъп..
        /// </summary>
        public static string UseAnotherServiceToLogIn {
            get {
                return ResourceManager.GetString("UseAnotherServiceToLogIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Потребителско име.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Потребители.
        /// </summary>
        public static string Users {
            get {
                return ResourceManager.GetString("Users", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Посещения.
        /// </summary>
        public static string VisitsLog {
            get {
                return ResourceManager.GetString("VisitsLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добре дошли на страницата на ТУ Фитнес. Това е интегрирана система за плащания и контрол на достъпа до мрежата на фитнеси към Технически Университет София..
        /// </summary>
        public static string WelcomeMemo {
            get {
                return ResourceManager.GetString("WelcomeMemo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to В системата сте, като .
        /// </summary>
        public static string YouAreLoggedInAs {
            get {
                return ResourceManager.GetString("YouAreLoggedInAs", resourceCulture);
            }
        }
    }
}
