﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models.Manage
{
    public class UserItemModel
    {
        public string UserName { get; set; }

        public string Name { get; set; }

        public string CardData { get; set; }

        public string CardNumber { get; set; }
    }
}