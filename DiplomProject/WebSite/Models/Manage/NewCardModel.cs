﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models.Manage
{
    public class NewCardModel
    {
        public string UserId { get; set; }

        public string CardId { get; set; }
    }
}