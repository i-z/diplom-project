﻿-- Insert DB initialization data
:r .\Initialize\Initialize.sql

BEGIN
	:r .\Seed\Seed.sql
END