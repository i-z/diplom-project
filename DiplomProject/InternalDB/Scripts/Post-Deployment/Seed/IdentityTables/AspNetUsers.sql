﻿-- dbo.AspNetUsers

INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'56e744bb-1619-41a7-9828-76083c50cbdb', N'Administrator', N'ALnz8NdzGrNt1tIhyTA7LsMkx4UPIu0YAr/HXHiqMea86czFbt6g/tzA9CYG5mi6+g==', N'b43130e9-3991-4de9-8651-f0245e4080fb', N'ApplicationUser', N'Administrator@tu.com', 7, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'001e2bfd-cac1-4584-8a49-93e9102fa019', N'ManagerPlovdiv', N'ABtbGY+QCT71PL5gh704ErQsWBgunuL8OJMapoXHTpUIZiQmHqOUPzfnWc0caXl7Hg==', N'd6b24fc9-1778-4bc0-955b-e026038fcad5', N'ApplicationUser', N'ManagerPlovdiv@tu.com', 1, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'6e46e6c5-ea32-49d7-a016-7db46650b102', N'ManagerSofia', N'AFpb1K3oGK4VkEUYN1yV5SkR+V1bWsSYNOCAPa82wG6H72m8rlKJxoGXS7qNXhnElA==', N'e57e1f02-2541-4970-a21e-bd11fb4cc923', N'ApplicationUser', N'ManagerSofia@tu.com', 2, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'f5cc5cb0-c9c0-4d8b-a46e-e67c471cbd95', N'ManagerSliven', N'ABEFU6fnihjPTPabPyGPsNoFXCNUPn7vhFKam0S7zjrPQgepWSFt5TJr8jSTQ9C2eQ==', N'64fd7454-d72b-4881-9d45-c5f76ec3f505', N'ApplicationUser', N'ManagerSliven@tu.com', 3, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'76aaf95a-9498-4624-9cd1-1df45e1ac9be', N'SubscribedClient', N'AJj4FHV68W3KGyZoIbqzJ4UycshGyVwaDDS1Kq21iUkwdmQ4ZGFldXlL36E6wIlsMA==', N'fb3b6a41-0b23-48ac-87cf-d4ee9dcce66e', N'ApplicationUser', N'SubscribedClient@gmail.com', 4, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'cab764af-8d7f-4186-bed6-727292697f75', N'CreditedClient', N'ABIM6TEFAzx165oQNnGgVxzwEMrtrXc6QQPL0rQJrebk5h1ul2JX+2RBz9XhedaIRw==', N'fee3d22e-43a0-42bf-8648-e6e96fcf1102', N'ApplicationUser', N'CreditedClient@gmail.com', 5, GETDATE())
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp] , [Discriminator], [Email], [PersonalDetailsId], [CreatedAt]) 
	VALUES (N'3cf4a1ed-a093-49f0-99ac-ec65db71d2b5', N'UncreditedClient', N'ABvXH6obmru4retVSYfoBQi2XJKw9iNj7AdlkpZXsOY1MN5aiabjuiEKyZUd3iY6TA==', N'fa92f962-0466-4da6-b0ba-d81260150011', N'ApplicationUser', N'UncreditedClient@gmail.com', 6, GETDATE())
